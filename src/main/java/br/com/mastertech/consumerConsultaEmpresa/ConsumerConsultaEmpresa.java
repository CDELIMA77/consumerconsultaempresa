package br.com.mastertech.consumerConsultaEmpresa;

import br.com.mastertech.producerConsultaEmpresa.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ConsumerConsultaEmpresa {

    @Autowired
    CnpjClient cnpjClient;

    @Autowired
    private KafkaTemplate<String, CnpjDTO> producer;

    @KafkaListener(topics = "spec2-cynthia-carvalho-2", groupId = "grupo-2")
    public void receber(@Payload Empresa empresa) {
        /*https://consultarcnpj.com.br/wp-content/plugins/cnpj-rf/receita-ws.php?cnpj=47960950000121*/
        CnpjDTO cnpjDTO = cnpjClient.getCnpj(empresa.getCnpj());
        if (Double.parseDouble(cnpjDTO.getCapital_social()) > 1000000000.00) { cnpjDTO.setStatus("Aprovado");} else { cnpjDTO.setStatus("Negado");}
        producer.send("spec2-cynthia-carvalho-3", cnpjDTO);
        System.out.println("gravado no Kafka" + cnpjDTO.getCnpj() + cnpjDTO.getStatus());
        }
}

