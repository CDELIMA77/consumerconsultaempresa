package br.com.mastertech.consumerConsultaEmpresa;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/*@FeignClient(name = "cnpj", url = "https://consultarcnpj.com.br/wp-content/plugins/cnpj-rf/receita-ws.php?cnpj=")*/
@FeignClient(name = "cnpj" , url = "https://www.receitaws.com.br/v1/cnpj/")
public interface CnpjClient {
    @GetMapping("{cnpj}")
    CnpjDTO getCnpj(@PathVariable String cnpj);
}